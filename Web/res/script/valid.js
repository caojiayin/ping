/* ==========================================================
 * valid.js
 * ==========================================================
 * $.valid('sitename,keyword')
 * $.valid({
     'field_id1':{
         'empty':'不能为空',
         'min|max':{error:'长度在1-2',val:[1,2]},
         'regex' : {error:'必须了字母',val:/^[a-z]+$/},
         'func' : function(val){
            if(val == 'ab'){
                return '不能是ab';
            }
            return true;
        }
     },
     'field_id2':{
        'empty':{error:'不能为空'},
     },
     'field_id3':''
 * })
 * ========================================================== */
;(function($, undefined) {
	

	$.fn.valid = function(options,wrapper) {
		var opts = {}
			,validator = null;
		wrapper = wrapper ? wrapper : '.form-group';
		if($.type(options) == 'string'){
			options = options.split(",");
			for(var i = 0; i < options.length; i++){
				opts[options[i]] = ['empty'];
			}
		}else{
			opts = options;
		}
		$form = this.eq(0);
		validator = $form.data('f_valid');
		if(!validator){
			validator = $form.data('f_valid') ? $form.data('f_valid') : new Valid($form, opts,wrapper);
			$form.data('f_valid', validator).attr('novalidate', 'novalidate');
		}
        //alert(options);
        if(options == null){
            return validator;
        }
		return validator.init();
	};
	
	function Valid($form, options,wrapper) {
		this.options = options;
		this.$form = $form;
		this.allowed_rules = [];
		this.errors = {};
		this.wrapper = wrapper;
		var self = this;		
		$.each(this.methods, function(k, v) {
			self.allowed_rules.push(k);
		});
        return this;
	};	
	$.extend(Valid.prototype, {
		init : function(){
			var  self = this
				,field = {}
				,normalized_rules = {};
			if (!$.isEmptyObject(this.errors)) {this.hide(this.errors);}	
			this.errors = {};
			$.each(self.options, function(field_id, rules) {
                normalized_rules = {};
				//console.log('field_id = '+field_id+',rules = '+rules);
                if(!$.trim(rules)){rules = ['empty'];}
				field.name = self.get_field_name(field_id);
				field.value = self.get_field_value(field_id);



				$.each(rules, function(rule_idx, rule_value) {
					//console.log('field_id = '+field_id+',rule_idx = '+rule_idx+',rule_value='+rule_value);

                    if($.isNumeric(rule_idx)){
                        if($.inArray(rule_value,self.allowed_rules) !== -1){
                            normalized_rules[rule_value] = null;
                        }
                    }else{
                        if(rule_idx.indexOf('|') == -1){
                            if($.inArray(rule_idx,self.allowed_rules) !== -1){
                                normalized_rules[rule_idx] = rule_value;
                            }
                        }else{
                            var idx = rule_idx.split("|");
                            for(var i = 0;i < idx.length; i++){
                                if($.inArray(idx[i],self.allowed_rules) !== -1){
                                    if($.type(rule_value) == 'object'){
                                        normalized_rules[idx[i]] = {
                                            'error' : rule_value.error,
                                            'val' : rule_value.val[i]
                                        };
                                    }else{
                                        normalized_rules[idx[i]] = rule_value;
                                    }

                                }
                            }
                        }
                    }
				});
				
				$.each(normalized_rules, function(fn_name, fn_args) {
                    var fn_args_val = '';
                    var fn_args_error = '';
                    //console.log(fn_name+','+fn_args+','+$.type(fn_args));
                    if($.type(fn_args) == 'object'){
                        fn_args_val = fn_args.val;
                        fn_args_error = fn_args.error;
                    }else{
                        fn_args_val = fn_args;
                        fn_args_error = fn_args;
                    }
					//console.log('field.name = '+field.name+',field.value = '+field.value+',fn_name='+fn_name);
					if (self.methods[fn_name].call(self, field.name, field.value, fn_args_val, normalized_rules) !== true) {
					    //console.log('fn_name = '+fn_name+',fn_args = '+fn_args);
						if(!self.errors[field_id]){
                            self.errors[field_id] = self.format.call(self, field.name, fn_name, fn_args_error);
                        }
					}
				});
			});
            if (!$.isEmptyObject(this.errors)){
				this._show(this.errors);
				return false;
			} else {
				return true;
			}
		},
		methods: {
			empty: function(field, value) {
				return value !== null && $.trim(value).length > 0;
			},
			min: function(field, value, min_len) {
				var length = $.trim(value).length
					,result = (length >= min_len);
				return result;
			},
			max: function(field, value, max_len) {
				return $.trim(value).length <= max_len;
			},
			regex: function(field, value, regexp) {
				return regexp.test(value);
			},
            func : function(field, value,func){
                if($.isFunction(func)){
                    var result = func(value);
                    if($.type(result) == 'string'){
                        if(!this.errors[field]){this.errors[field] = result;}
                        return false;
                    }
                    return result;
                }
                return true;
            }
		},
		format: function(field_id, rule, params) {
            //console.log('field_id = '+field_id+',rule = '+rule+',params = '+params);
            if($.type(params) == 'string'){
                return params;
            }
            return '';
        },

        show : function(obj){
            obj = typeof obj  == 'string' ? $(obj) : obj;
            var span = '<span class="icon-remove form-control-feedback"></span>';
            $(span).insertAfter(obj);
            obj.bind('focus click change',function(){
                obj.closest(self.wrapper).removeClass('has-error has-feedback');
                obj.removeClass('has-error has-feedback');
                $(obj.attr("data-show")).removeClass('has-error has-feedback');
                obj.parent().find(".form-control-feedback").remove();
            });
        },
		_show : function(errors){
			var self = this;
			$.each(errors, function(k, v) {
				var $input = self.$form.find('#'+k);
				if (self.wrapper !== null) {
					$input.closest(self.wrapper).addClass('has-error has-feedback');
				}
                var span = '<span class="icon-remove form-control-feedback"></span>';
                $(span).insertAfter($input);
                $input.bind('focus click change',function(){
                    $input.closest(self.wrapper).removeClass('has-error has-feedback');
                    $input.removeClass('has-error has-feedback');
                    $($input.attr("data-show")).removeClass('has-error has-feedback');
                    $input.parent().find(".form-control-feedback").remove();
                });
			});			
		},
		hide : function(errors){
			var self = this;
			$.each(errors, function(k, v) {
				var $input = self.$form.find('#'+k);
				if (self.wrapper !== null) {
                    $input.closest(self.wrapper).removeClass('has-error has-feedback');
				}
                $input.removeClass('has-error has-feedback');
                $($input.attr("data-show")).removeClass('has-error has-feedback');
                $input.parent().find(".form-control-feedback").remove();
			});
		},
		get_field_value: function(field_id) {
			var $input = self.$form.find('#'+field_id);
			if ($input.is('[type="checkbox"], [type="radio"]')) {
				return $input.is(':checked') ? $input.val() : null;
			} else {
				return $input.val();
			}
		},
		get_field_name: function(field_id){
			var $input = self.$form.find('#'+field_id);
			return $input.attr("name");
		}
	});
	
})(jQuery);