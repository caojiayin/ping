

var Message = function(status,text,caller,autoClose){
    text = text ? text : "";
    var MessageTemplate = $('<div class="text-center">' +
        '<div class="i-circle"><i class="icon-4x"></i></div>' +
        '<h4></h4>' +
        '<p style="margin-top: 10px;">'+text+'</p>' +
        '</div>');
    if(status == 1){
        MessageTemplate.find("h4").addClass("text-success").text("操作成功");
        MessageTemplate.find(".icon-4x").addClass("icon-ok");
        MessageTemplate.find(".i-circle").addClass("text-success");
    }else if(status == 2){
        MessageTemplate.find("h4").addClass("text-danger").text("操作失败");
        MessageTemplate.find(".icon-4x").addClass("icon-remove");
        MessageTemplate.find(".i-circle").addClass("text-danger");
    }else{
        MessageTemplate.find("h4").addClass("text-info").text("请确认要执行些操作吗？");
        MessageTemplate.find(".icon-4x").addClass("icon-question-sign");
        MessageTemplate.find(".i-circle").addClass("text-info");
    }
    var messageDialog = window.top.xDialog({
        'showbg' : false,
        'content' : '<div class="text-center">'+MessageTemplate.html()+'</div>'
    });
    if(autoClose){
        setTimeout(function(){
            messageDialog.close();
            if(caller && $.isFunction(caller)){
                caller();
            }
        },autoClose*1000);
    }
};


function EventFunction(pObj){
    pObj = pObj || null;
    $("*[data-toggle=dialog]",pObj).each(function(){
        var href = $(this).attr('href');
        var enter = $(this).attr('data-dialog-enter');
        var cancel = $(this).attr('data-dialog-cancel');
        var option = $(this).attr('data-dialog-option');
        var title = $(this).attr('title');
        title = title ? title : $(this).text();
        var btn = {};
        if(enter != 'false'){
            btn[' 确 定 :btn btn-primary'] = function(d,b){
                try{
                    if($.trim(enter)){
                        enter = tofunc(enter);
                        enter(d,b);
                    }else{
                        dialogEnter(d,b);
                    }
                }catch (e){
                    var handle = d.template.find(".dialog-handle");
                    if(handle.length > 0){
                        handle.find("[type=submit]").eq(0).click();
                    }
                }
            }
        }if(cancel != 'false'){
            btn[' 取 消 :btn btn-default'] = function(d,b){
                try{
                    if($.trim(cancel)){
                        cancel = tofunc(cancel);
                        cancel(d,b);
                    }else{
                        dialogCancel(d,b);
                    }
                }catch (e){
                    d.close();
                }
            }
        }
        var config = {
            'title' : title,
            'content' : 'url:'+href,
            'btns' : btn,
            'complete' : function(o){
                try{
                    EventFunction(o.template);
                    o.template.find(".dialog-handle").hide();
                }catch (e){}
            }
        };
        if(option){
            option = option.split(",");
            for(var k in option){
                var _t = option[k];
                if(_t && typeof _t == 'string'){
                    _t = _t.split(":");
                    config[_t[0]] = _t[1];
                }
            }
        }
        var id = $(this).attr("id");
        if(!id){
            id = $.md5("_"+Math.random(99,9999)+'_'+new Date().getTime());
            $(this).attr("id",id);
        }
        $("#"+id).xDialog(config);
    });


    $(".checkAll").each(function(){
        $(this).click(function(){
            var self = $(this);
            var id = self.attr("id");
            console.log(self.is(":checked"));
            if(self.is(":checked")){
                $("input[data-parent="+id+"]").prop("checked","checked");
            }else{
                $("input[data-parent="+id+"]").removeProp("checked");
            }
        });
    });
}
function tofunc(str) {
    try {
        return eval('(' + str + ')');
    } catch(e) {
        return {};
    }
}

(function($){
    $.fn.submitForm = function(before, callback) {
        var $this = $(this);
        if ($this.length == 0){return false;}
        var action = $this.attr("action");
        $this.submit(function() {
            var submit_btn = $this.find(":submit").eq(0);
            var status = $this.attr("submitStatus");
            var btnVal = submit_btn.text();
            if (status) {return false;}
            $this.attr('submitStatus', 'true');
            if ($.isFunction(before)) {
                var bfRet = before($this);
                if (!bfRet) {
                    $this.removeAttr("submitstatus");
                    return false;
                }
            }
            $this.find("input,textarea,select").attr("readonly",'readonly');
            submit_btn.html('<i class="icon-refresh icon-spin"></i> 提交中...');
            $.ajax({
                type: 'POST',
                url: action,
                data: $this.serialize(),
                dataType: 'JSON',
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    $this.find("input,textarea,select").removeAttr("readonly");
                    $this.removeAttr("submitstatus");
                    submit_btn.removeAttr("disabled").text(btnVal);
                    var responseText = XMLHttpRequest.responseText;
                    alert(errorThrown+XMLHttpRequest.status+responseText);
                },
                success: function(result) {
                    $this.find("input,textarea,select").removeAttr("readonly");
                    $this.removeAttr("submitstatus");
                    submit_btn.removeAttr("disabled").text(btnVal);
                    if(result && result.status == 1){
                        $this.get(0).reset();
                    }
                    if(callback && $.isFunction(callback)){
                        callback(result,$this);
                    }else if(result){
                        if(result.status == 1){
                            var modal = $this.parents(".modal");
                            if(modal.length > 0 && modal.attr("data-dialog")){
                                var dialog = modal.attr("data-dialog");
                                $("#"+dialog).xDialog("close");
                            }
                            Message(result.status,result.info,function(){
                                if(result.url){
                                    location.href = result.url;
                                }
                            },1);
                        }else{
                            Message(2,result.info);
                        }

                    }
                }
            });
            return false;
        });
    };
    $.fn.ajaxDelete = function (callback) {
        $(this).click(function(){
            var self = $(this);
            self.addClass("label-danger");
            var href = self.attr('href');
            var title = self.attr("title") ? self.attr("title") : '执行数据删除操作';
            var params = self.attr("data-params");
            var btn = {};
            btn[' 确 定 :btn btn-primary'] = function(d,b){
                b.siblings().attr("disabled","disabled");
                b.html('<i class="icon-refresh icon-spin"></i>').attr("disabled","disabled");

                $.ajax({
                    'dataType' : 'json',
                    'async' : false, //同步请求
                    'data' : {'value':params},
                    'type' : 'post',
                    'url' : href,
                    'error':function(XMLHttpRequest, textStatus, errorThrown){
                        d.close();
                        self.removeClass("label-danger");
                        if(XMLHttpRequest.status == 200){
                            alert("操作成功，ajax接收返回数据格式不正确!\n\n"+XMLHttpRequest.statusText+"\n"+XMLHttpRequest.responseText);
                        }else{
                            alert("Ajax处理时出错\n"+textStatus+"\n"+errorThrown);
                        }
                    },
                    'success' : function(result){
                        d.close();
                        if(result.status == 1){
                            Message(1,'',null,1);
                            $(self.attr("data-toggle")).fadeOut(200,function(){
                                $(this).remove();
                            })
                        }else{
                            self.removeClass("label-danger");
                            Message(2,result.info);
                        }
                    }
                });
            }
            btn[' 取 消 :btn btn-default'] = function(d){
                self.removeClass("label-danger");
                d.close();
            }
            xDialog({
                'hasclose' : false,
                'width' : 300,
                'title' : '系统提示',
                'content' : '<p style="font-weight: 700;font-size: 16px;" class="txt-wran">你确定要执行以下操作吗？</p><p style="padding:5px 0 0 24px;font-size: 14px;" class="txt-danger">'+title+'</p>',
                'btns' : btn
            });
            return false;
        });
        return false;
    };
})(jQuery);
$(function(){
    $.ajaxSetup({
        type: "POST",
        error: function(jqXHR, textStatus, errorThrown){
            switch (jqXHR.status){
                case(500):
                    alert("Ajax处理时出错，服务器系统内部错误");break;
                case(404):
                    alert("Ajax处理时出错，服务器无法响应");break;
                case(403):
                    alert("Ajax处理时出错，无权限执行此操作");break;
                case(408):
                    alert("Ajax处理时出错，请求超时");break;
                default:
                    alert("statusText:"+jqXHR.statusText+"\n"+jqXHR.responseText);
            }
        }
    });
    $(".ajaxdelete").each(function(){
        $(this).ajaxDelete();
    });
    EventFunction();
});

function formatSize(size){
    var a = ["B", "KB", "MB", "GB", "TB", "PB"];
    var pos = 0;
    while (size >= 1024) {
        size /= 1024;
        pos++;
    }
    var num = new Number(size);
    //console.log(num.toFixed(2));
    return num.toFixed(2)+" "+a[pos];
}
Array.prototype.removeRepeat = function() {
    var t, b = [],
        _i = this.length;
    for (var i = 0; i < _i - 1; i++) {
        for (var j = i + 1; j < _i; j++) {
            if (this[j] === this[i]) {
                this.splice(j, 1);
                if (this[i] !== t) t = this[i],
                    b.push(this[i]);
                i--,
                    _i--
            }
        }
    };
    return b;
};
Array.prototype.min = function() {
    return Math.min.apply({},
        this);
};
Array.prototype.max = function() {
    return Math.max.apply({},
        this);
};
Array.prototype.indexOf = function(val) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] == val) return i;
    };
    return - 1;
};
Array.prototype.remove = function(val) {
    var index = this.indexOf(val);
    if (index > -1) {
        this.splice(index, 1);
    }
};
Number.prototype._toFixed = function(s) {
    return (parseInt(this * Math.pow(10, s) + 0.5) / Math.pow(10, s)).toString();
};

function randString(obj,length){
    length = length || 6;
    var chars = 'abcdefghijklmnopqrstuvwxyz1234567890';
    var maxPos = chars.length;
    var str = '';
    for(var i = 0;i < length;i++){
        str += chars.charAt(Math.floor(Math.random() * maxPos));
    }
    if(obj){
        if(typeof obj == 'string'){
            obj = $(obj);
        }
        obj.val(str);
    }else{
        return str;
    }
};

function tofunc(str) {
    try {
        return eval('(' + str + ')');
    } catch(e) {
        return {};
    }
};