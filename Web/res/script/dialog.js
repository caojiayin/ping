/**
 * Created by jiayin on 14-4-25.
 */

;(function ($, window, undefined) {
    var xDialog = function (options) {
        options = options || {};
        if(typeof options === 'object'){
            for (var i in xDialog.defaults) {
                if (options[i] === undefined) options[i] = xDialog.defaults[i];
            }
        }
        var selector = null;
        try{
            selector = $(this).attr("id");
        }catch(e){}
        if(!selector){
            selector = $.md5("_"+Math.random(99,9999)+'_'+new Date().getTime());
        }
        if(xDialog.data[selector]){
            return xDialog.data[selector];
        }
        options['__ID__'] = selector;
        return xDialog.data[selector] = new xDialog.fn._init(options);
    };

    xDialog.fn = xDialog.prototype = {
        _init: function (options) {
            this.options = options;
            options.init && options.init.call(self, window);
            this._template();
            this._listenEvent();
            this.content(options.content);
            return this;
        },
        content : function(str){
            if(!str || str == ''){return;}
            var self = this;
            var regx = /^(http|https):\/\/(([0-9]{1,3}\.){3}[0-9]{1,3}|([0-9a-z_!~*'()-]+\.)*([0-9a-z][0-9a-z-]{0,61})?[0-9a-z]\.[a-z]{2,6})(:[0-9]{1,4})?((\/?)|(\/[0-9a-z_!~*'().;?:@&=+$,%#-]+)+\/?)$/ig;
            var isUrl = !!regx.test(str);
            if(str.substr(0,4) == 'url:'){
                isUrl = true;
                str = str.substr(4);
            }
            if(isUrl){
                self._getContent(str);
            }else{
                if(this.options.hasclose){
                    this.template.find(".close").show();
                    this.template.find(".modal-header").show();
                }
                this._setContent(str);
                this._setBtn();
                this._completed();
            }
        },


        close : function(){
            var self = this,
                fn = self.options.close;
            if($.isFunction(fn) && fn.call(self,window) === false){
                return self;
            }
            var ajaxObj = this.template.data('ajaxObj');
            if(ajaxObj){try{ajaxObj.abort(false);}catch (e){}}
            $('body').css({'overflow-y' : 'auto'});
            self.template_mask.toggleClass("in fade");
            self.template.animate({
                'opacity' : 0,
                'top' : -1000
            },200,function(){
                $(this).remove();
            });
            this.template_mask.fadeOut(100,function(){$(this).remove();});
            var id = this.options['__ID__'];
            this.options = {};
            for (var ii in self) {
                if (self.hasOwnProperty(ii) && ii !== 'Dom') delete self[ii];
            }
            for(var d in xDialog.data){
                if(d == id){
                    delete xDialog.data[d];
                    break;
                }
            }
            try{xDialog.ajaxObj.abort(false);}catch (e){}
        },

        _setBtn : function(){
            var self = this;
            var btns = self.options.btns;
            var btnWrap = self.template.find(".modal-footer");
            if(btns && typeof btns == 'object'){
                $.each(btns, function(name, props) {
                    if(name && props){
                        var nameText,className = self.options.btnclass;
                        if(name.indexOf(":") != -1){
                            nameText = name.substring(0,name.indexOf(":"));
                            className = name.substring(name.indexOf(":")+1,name.length);
                        }else{
                            nameText = name;
                        }
                        var button = $('<button class="'+className+'">'+nameText+'</button>');
                        if(props && $.isFunction(props)){
                            button.click(function(){
                                props(self,button);
                            })
                        }
                        button.appendTo(btnWrap);
                    }
                });
                btnWrap.show();
            }else if(typeof btns == 'string'){
                btnWrap.show().html(btns);
            }else{
                btnWrap.hide();
            }
        },

        _getContent : function(url){
            var self = this;
            var data = {};
            data['dialog_index'] = this.options.__ID__;
            data['__'] = new Date().getTime();
            this._setContent('<div style="width: 140px;text-align: center"><i class="icon-refresh icon-spin"></i> 加载中，请稍后...</div>');
            xDialog.ajaxObj = $.ajax({
                //'async' : false, //同步请求
                'data' : data,
                'type' : 'GET',
                'url' : url,
                'error':function(XMLHttpRequest, textStatus, errorThrown){
                    self.close();
                    if(textStatus == 'abort'){return false;}
                    alert(textStatus+errorThrown);

                },
                'success' : function(result){
                    self._setContent(result);
                    self._setBtn();
                    if(self.options.hasclose){
                        self.template.find(".close").show();
                        self.template.find(".modal-header").show();
                    }
                    xDialog.ajaxObj = null;
                    self._completed();
                }
            });
        },
        _setContent : function(content){
            var width = 0;
            if(this.options.width > 0){
                width = this.options.width;
                this.template.find(".modal-body").html(content);
            }else{
                var _tmp = $('<div>'+content+'</div>');
                _tmp.hide().appendTo($('body'));
                width = _tmp.outerWidth(true);
                this.template.find(".modal-body").html('');
                _tmp.appendTo(this.template.find(".modal-body")).show();
            }
            width = Math.max(width,300);
            this.template.find('.modal-dialog').width(width+30).height(this.options.height);
            var tmpWidth = this.template.find(".modal-body").outerWidth(true);
            var tmpHeight = this.template.outerHeight(true);
            var bodyWidth = $(window).width();
            var bodyHeight = $(window).height();
            if(!this.options.title){
                this.template.find(".modal-header").addClass("no-header");
            }

            this.template.css({
                'top' : -tmpHeight
            }).animate({
                'opacity' : 1,
                'top' : 0
            },200).show();
        },
        _completed : function(){
            this.options.complete && this.options.complete.call(this, this);
        },
        _listenEvent : function(){
            var self = this;
            //监听关闭按钮
            self.template.bind('click',function(event){
                var target = event.target;
                var _target = $(target);
                if(_target.hasClass('close')){
                    self.close();
                }
            });
        },
        _template : function(){
            var template = '<div class="modal"><div class="modal-dialog">'+
                '<div class="modal-content">'+
                '<div class="modal-header no-header"><span class="close">&times</span></div>'+
                '<div class="modal-body"></div>'+
                '<div class="modal-footer"></div>'+
                '</div></div></div>';
            this.template  = $(template);
            this.template_mask = $('<div class="modal-backdrop"></div>');
            this.template.appendTo($('body'));
            this.template.attr("data-dialog",this.options.__ID__);
            if(this.options.showbg) {
                this.template_mask.appendTo($('body')).addClass("in");
            }
            this.template.find(".close").hide();
            if(this.options.hasclose == false && !this.options.title){
                this.template.find(".modal-header").hide();
            }
            if(!this.options.btns || this.options.btns.length == 0){
                this.template.find(".modal-footer").hide();
            }
            $('body').css({'overflow-y' : 'hidden'});
        }

    };
    xDialog.fn._init.prototype = xDialog.fn;
    $.fn.dialog = $.fn.xDialog = function () {
        var config = arguments;
        var selector = $(this).attr("id");
        if (xDialog.data[selector]) {
            var method = xDialog.data[selector];
            var c = config[0];
            if(c == 'close'){
                method.close();
            }
        }else {
            this['bind']('click', function () {
                xDialog.apply(this, config);
                return false;
            });
        }
        return this;
    };
    xDialog.data = [];
    xDialog.defaults = {
        title		: '',
        content		: '', //内容(可选内容为){ text |  url };
        iframe      : false,
        width		: 'auto',
        height		: 'auto',
        css         : '',
        data        : {},//ajax传递数据
        hasclose	: true, //是否显示关闭按钮
        showbg		: true,
        btns		: null, //按钮组{'确定'  : function(){}}
        btnclass    : 'btn btn-primary',
        zindex      : 999,
        init        : null, //初始化后执行函数
        close       : null, // 关闭时执行的函数
        complete    : null //内容加载完成后调用
    };
    window.xDialog = $.dialog = $.xDialog = xDialog;
}(this.xDialog || this.jQuery && (this.xDialog = jQuery), this));

