<?php
/**
 * +----------------------------------------------------------------------
 * | Author:  jiayin <caojiayin1984@gmail.com>
 * +----------------------------------------------------------------------
 * | 2015/03/20
 * +----------------------------------------------------------------------
 * | BaseService.class.php
 * +----------------------------------------------------------------------
 * | 后台管理基类
 * +----------------------------------------------------------------------
 **/

namespace Home\Service;
use Think\Controller;

class BaseService extends Controller{
    public $login_uid = 0;
    public $login_user = 0;
    public function __construct(){
        parent::__construct();
        $this->_hasLogin();
    }

    private function _hasLogin(){
        $uid = session('USER_CENTER_UID');
        $user = session('USER_CENTER_INFO');
<<<<<<< HEAD
	if(!$uid || !$user){
=======
        if(!$uid || !$user){
>>>>>>> 3553d56ebfe9e85c5286a5be35235080f03ba0b7
            if(IS_AJAX){
                $data           =   array();
                $data['info']   =   '请登录后再进行操作';
                $data['status'] =   999;
                $data['url']    =   U('Passport/login');
                $this->ajaxReturn($data);
            }else{
                if(strtolower(CONTROLLER_NAME) == 'index' && strtolower(ACTION_NAME) == 'index'){
<<<<<<< HEAD
			redirect(U('Passport/login'));
=======
                    header('Location:'.U('Passport/login'));
>>>>>>> 3553d56ebfe9e85c5286a5be35235080f03ba0b7
                }else{
                    $this->error('请登录后再进行操作',U('Passport/login'),false);
                }
            }
            exit;
        }
        $this->login_uid = $uid;
        $this->login_user = $user;
        $this->assign('login_uid',$uid);
        $this->assign('login_user',$user);
    }

}