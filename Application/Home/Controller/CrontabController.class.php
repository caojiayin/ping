<?php
namespace Home\Controller;
use Think\Controller;
class CrontabController extends Controller
{
    private $model = null;
    private $maxLimit = 100;//每次读取多少条数据
    private $currentIndex = 1;

    public function __construct(){
        parent::__construct();
        $this->model = D("Site");
        echo "[".date("Y-m-d H:i:s")."]Start checking the site status\r\n";
        echo "maxLimit : {$this->maxLimit}\r\n";
    }

    public function run2(){
        $ip = 'baidu.com';
        $ping = '';
        //linux
        if (PATH_SEPARATOR==':'){
            exec("ping $ip -c 4",$info);
            if (count($info) < 9){
                return '服务器无法连接';
            }
            //获取ping的时间
            $str = $info[count($info)-1];
            $ping = round(substr($str, strpos($str,'/',strpos($str,'='))+1 , 4));
        }
        else //windows
        {
            exec("ping $ip -n 4",$info);
            if (count($info) < 10){
                return '服务器无法连接';
            }
            //获取ping的时间
            $str = $info[count($info)-1];
            $ping = mb_substr($str,  strripos($str,'=')+1);
        }
        return $ping;

    }
    public function run(){
        $list = $this->model->where(array('status' => 'Y'))->limit($this->maxLimit)->page($this->currentIndex)->select();
        echo "The query data total:".count($list).",Index={$this->currentIndex}\r\n";
        if($list){
            $this->currentIndex++;
            foreach($list as $val) {
                echo "ping {$val['website']},{$val['port']} ...";
                $ping = $this->_getPing($val['website']);
                $http_status = $this->_getStatus($val['website']);
                $this->model->where(array('id' => $val['id']))->save(array('last_time'=> time(),'ping' => $ping,'http_status' => $http_status));
                M("sites_item")->add(array('sites_id' => $val['id'],'atime' => time(),'ping' => $ping,'http_status' => $http_status));
                echo "[$ping],[$http_status]\r\n";
            }
            $this->run();
        }else{
            echo "[".date("Y-m-d H:i:s")."]Check is completed";
        }
    }

    private function _getPing($url){
        $ping = -1;
        if (PATH_SEPARATOR==':'){ //linux
            exec("ping $url -c 4",$info);
            if (count($info) < 9){
                return '服务器无法连接';
            }
            $str = $info[count($info)-1];
            $ping = round(substr($str, strpos($str,'/',strpos($str,'='))+1 , 4));
        }
        else{ //windows
            exec("ping $url -n 4",$info);
            if (count($info) < 10){
                return '服务器无法连接';
            }
            //获取ping的时间
            $str = $info[count($info)-1];
            $ping = mb_substr($str,  strripos($str,'=')+1);
        }
        return intval($ping) / 1000;
    }

    private function _getStatus($url){
        $ch = curl_init ();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 200);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_NOBODY, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, FALSE);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_TIMEOUT,60); //超时60秒
        curl_exec($ch);
        $httpCode = curl_getinfo($ch,CURLINFO_HTTP_CODE);
        var_dump($httpCode);
        return $httpCode;
    }


    private function _check($url,$port = 80){
        $val = -1;
        $type= "/x08";
        $code= "/x00";
        $checksum= "/x00/x00";
        $identifier = "/x00/x00";
        $seqNumber = "/x00/x00";
        $data= "Scarface";
        $package = $type.$code.$checksum.$identifier.$seqNumber.$data;
        $checksum = $this->_icmpChecksum($package);
        $commonProtocol = getprotobyname("tcp");
        $socket = socket_create(AF_INET, SOCK_RAW, 0);
        if ($socket === false) {
            $errorcode = socket_last_error();
            $errormsg = socket_strerror($errorcode);
            echo $errormsg;
        }
        socket_set_option($socket,SOL_SOCKET,SO_RCVTIMEO,array("sec"=>10, "usec"=>0 ) );  // 发送超时2秒
        socket_set_option($socket,SOL_SOCKET,SO_SNDTIMEO,array("sec"=>10, "usec"=>0 ) );  // 接收超时4秒
        socket_connect($socket, $url, null);
        $startTime = microtime(true);
        socket_send($socket, $package, strLen($package), 0);
        if (socket_read($socket, 255)) {
            $val = round(microtime(true) - $startTime, 4);
        }
        socket_close($socket);
        return $val;
    }
    private function _icmpChecksum($data){
        if (strlen($data)%2)
            $data .= "/x00";
        $bit = unpack('n*', $data);
        $sum = array_sum($bit);

        while ($sum >> 16)
            $sum = ($sum >> 16) + ($sum & 0xffff);

        return pack('n*', ~$sum);
    }
}

