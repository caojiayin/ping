<?php
namespace Home\Controller;
use Think\Controller;
use Home\Service\BaseService;

class PersonalController extends BaseService {
    private $model = null;
    public function __construct()
    {
        parent::__construct();
        $this->model =  D("Manager");
    }

    public function modifypw(){
        if(IS_POST){
            $opassword = I('post.opassword',null);
            $npassword = I('post.npassword',null);
            $rpassword = I('post.rpassword',null);
            if(!$opassword){$this->error("请输入原密码");}
            if(!$npassword){$this->error("请输入新密码");}
            if(!$rpassword){$this->error("请重复输入新密码");}
            if($npassword != $rpassword){$this->error("两次输入的新密码不相同");}

            $user = $this->model->where(array('id'=> $this->login_uid))->find();

            if($this->model->enPassword($opassword,$user['salt']) != $user['password']){
                $this->error("原密码输入错误");
            }
            $this->model->where(array('id' => $this->login_uid))->save(array(
                'password' => $npassword
            ));
            $this->success("密码更新成功");
        }
        $this->display();
    }
}