<?php
namespace Home\Controller;
use Think\Controller;
use Home\Service\BaseService;

class ManagerController extends BaseService {
    private $model = null;
    public function __construct()
    {
        parent::__construct();
        if($this->login_user['role_id'] != 1){
            $this->error("无权限访问");exit;
        }
        $this->model =  D("Manager");
    }

    public function index(){
        $website = I("get.website");
        $page = I("page",0,'intval');
        if($website){
            $this->model->where(array('website' => array('like','%'.$website.'%')));
        }
        $this->list = $this->model->page($page)->limit(20)->select();
        $this->total = $this->model->count();
        $this->page = page($page,$this->total,array(),20);
        $this->website = $website;
        $this->display();
    }

    public function add(){
        if(IS_POST){
            $whereId = I('post.whereid',0,'intval');
            if(!$data = $this->model->create(null,$whereId ? \Think\Model::MODEL_UPDATE : \Think\Model::MODEL_INSERT)){
                $this->error($this->model->getError());
            }
            if($whereId){
                if(false !== $this->model->where(array('id'=>$whereId,'is_default' => 'N'))->save($data)){
                    $this->success('编辑成功',U('Manager/index'));
                }else{
                    $this->error($this->model->getError());
                }
            }else{
                if(false !== $this->model->add($data)){
                    $this->success('添加成功',U('Manager/index'));
                }else{
                    $this->error($this->model->getError());
                }
            }
            return;
        }
        $id = I("id",0,'intval');
        if($id){
            $this->result = $this->model->where(array('id' => $id))->find();
        }
        $this->display();
    }

    public function del(){
        if(IS_POST){
            $id = I('post.value',0,'intval');
            if(!$id){
                $this->error('未接收到数据');
            }
            $this->model->where(array('id' => $id,'is_default' => 'N'))->delete();
            $this->success("删除成功");
        }else{
            $this->error('未接收到数据');
        }
    }
}