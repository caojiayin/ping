<?php
/**
 * +----------------------------------------------------------------------
 * | Author:  jiayin <caojiayin1984@gmail.com>
 * +----------------------------------------------------------------------
 * | 2015/06/17 16:43
 * +----------------------------------------------------------------------
 * | AdvDataModel.class.php
 * +----------------------------------------------------------------------
 * |
 * +----------------------------------------------------------------------
 **/
namespace Home\Model;
use Think\Model;
class ManagerModel extends Model{
    protected $tableName = 'manager';
    protected $_validate = array(
        array('username','require','用户名不能为空！',Model::EXISTS_VALIDATE),
        array('name','require','姓名称不能为空！',Model::MODEL_BOTH),
    );
    protected $_auto = array(
        array('created_time','time',Model::MODEL_INSERT,'function'),
    );


    public function enPassword($password,$salt){
        return md5(md5($password.md5($salt)).md5($password).md5(substr(md5($password.$salt),5,10).md5($salt.$password)));
    }

    protected function _before_insert(&$data,$options){
        if(!isset($data['password']) || empty($data['password'])){
            $this->error = '密码不能为空!';
            return false;
        }
        $salt = \Org\Util\String::randString(6,1);
        $data['password'] = $this->enPassword($data['password'],$salt);
        $data['salt'] = $salt;
        return true;
    }

    protected function _before_update(&$data,$options){
        $id = isset($data['id']) ? $data['id'] : $options['where']['id'];
        if(!$id){return false;}
        if(isset($data['password']) && !empty($data['password'])){
            $salt = \Org\Util\String::randString(6,1);
            $data['password'] = $this->enPassword($data['password'],$salt);
            $data['salt'] = $salt;
        }else{
            unset($data['password']);
        }
        return true;
    }

    public function doLogin($username,$password){
        $userInfo = $this->where(array('username' => $username))->find();
        if(!$userInfo){
            $this->error = '用户名或密码输入错误!1';
            return false;
        }
        $uPassword = $userInfo['password'];
        $cPassword = $this->enPassword($password,$userInfo['salt']);
        if($uPassword != $cPassword){
            $this->error = '用户名或密码输入错误';
            return false;
        }
        $this->setLogin($userInfo);
        $this->where(array('id' => $userInfo['id']))->save(array(
            'last_time' => time(),
            'last_ip' => get_client_ip(),
        ));
<<<<<<< HEAD
        return $userInfo;
=======
        return true;
>>>>>>> 3553d56ebfe9e85c5286a5be35235080f03ba0b7
    }
    private function setLogin($user){
        session('USER_CENTER_INFO',$user);
        session('USER_CENTER_UID',$user['id']);
<<<<<<< HEAD
        //cookie('uid',$user['id']);
=======
        cookie('uid',$user['id']);
>>>>>>> 3553d56ebfe9e85c5286a5be35235080f03ba0b7
    }
    public  function logout($user){
        session('USER_CENTER_INFO',null);
        session('USER_CENTER_UID',null);
<<<<<<< HEAD
        //cookie('uid',null);
=======
        cookie('uid',null);
>>>>>>> 3553d56ebfe9e85c5286a5be35235080f03ba0b7
    }

}