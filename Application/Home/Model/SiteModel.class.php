<?php
/**
 * +----------------------------------------------------------------------
 * | Author:  jiayin <caojiayin1984@gmail.com>
 * +----------------------------------------------------------------------
 * | 2015/06/17 16:43
 * +----------------------------------------------------------------------
 * | AdvDataModel.class.php
 * +----------------------------------------------------------------------
 * |
 * +----------------------------------------------------------------------
 **/
namespace Home\Model;
use Think\Model;
class SiteModel extends Model{
    protected $tableName = 'sites';
    protected $_validate = array(
        array('webname','require','网站名称不能为空！',Model::MUST_VALIDATE),
        array('website','require','网站地址不能为空！',Model::MUST_VALIDATE),
    );
    protected $_auto = array(
        array('atime','time',Model::MODEL_INSERT,'function'),
    );



}