<?php
/**
 * +----------------------------------------------------------------------
 * | Author:  jiayin <caojiayin1984@gmail.com>
 * +----------------------------------------------------------------------
 * | 2015/03/20
 * +----------------------------------------------------------------------
 * | ChannelController.class.php
 * +----------------------------------------------------------------------
 * | 项目公共函数库
 * +----------------------------------------------------------------------
 **/

/**
+----------------------------------------------------------
 * 在数据列表中搜索
+----------------------------------------------------------
 * @access public
+----------------------------------------------------------
 * @param array $list 数据列表
 * @param mixed $condition 查询条件
 * 支持 array('name'=>$value) 或者 name=$value
+----------------------------------------------------------
 * @return array
+----------------------------------------------------------
 */
function list_search($list,$condition) {
    if(is_string($condition))
        parse_str($condition,$condition);
    // 返回的结果集合
    $resultSet = array();
    foreach ($list as $key=>$data){
        $find   =   false;
        foreach ($condition as $field=>$value){
            if(isset($data[$field])) {
                if(0 === strpos($value,'/')) {
                    $find   =   preg_match($value,$data[$field]);
                }elseif($data[$field]==$value){
                    $find = true;
                }
            }
        }
        if($find)
            $resultSet[]     =   &$list[$key];
    }
    return $resultSet;
}

/**
 * 根据某个值进行排序
 */
function arraySortByVal($list,$key,$order='asc',$limit = 0){
    $len = count($list);
    for($i = 1; $i < $len; $i++){
        $flag = false;
        for($j = $len-1; $j >= $i; $j--){
            if($order == 'asc'){
                if(intval($list[$j][$key]) < intval($list[$j-1][$key])){
                    $x = $list[$j];
                    $list[$j] = $list[$j-1];
                    $list[$j-1] = $x;
                    $flag = true;
                }
            }else{
                if(intval($list[$j][$key]) > intval($list[$j-1][$key])){
                    $x = $list[$j];
                    $list[$j] = $list[$j-1];
                    $list[$j-1] = $x;
                    $flag = true;
                }
            }

        }
        if(! $flag){break;}
    }
    if($limit > 0 ){
        return array_slice($list,0,$limit);
    }
    return $list;
}

/**
 * 一级数组生成树结构
 * @param $data 数据
 * @param $pk 匹配字段
 * @param $ck 匹配字段
 * @return array
 */
function array_to_tree($data,$pk,$parent_key,$result = 'array',$opt = null){
    $items = array();
    foreach($data as $key => $val){
        $items[$val[$pk]] = $val;
    }
    $tree = array();
    foreach($items as $key => $item){
        if($item[$parent_key] && isset($items[$item[$parent_key]])){
            $items[$item[$parent_key]]['_child_'][] = &$items[$key];
        }else{
            $tree[] = &$items[$key];
        }
    }
    if($result == 'array'){
        return $tree;
    }else{
        return array_to_select($tree,$pk,0,$opt);
    }
}

/**
 * 将树转成Select
 * @param $tree
 * @param $pk
 * @param int $level
 * @param null $opt
 * @return string
 */
function array_to_select($tree,$pk,$level = 0,$opt = null){
    $result = '';
    $opt = $opt ? $opt : array('selected' => '','text' => 'name');
    if(!is_array($tree)){return $tree;}
    foreach($tree As $key => $val){
        $_level = $level;
        $before = str_pad('',$_level*3,'　',STR_PAD_LEFT);
        $result .= '<option value="'.$val[$pk].'" '.(($opt['selected'] && $opt['selected'] == $val[$pk]) ? 'selected' : '').'>'.($_level > 0 ? ($before.'├ ') : '').$val[$opt['text']].'</option>';
        if(isset($val['_child_']) && $val['_child_']){
            $_level++;
            $result .= array_to_select($val['_child_'],$pk,$_level,$opt);
        }
    }
    return $result;
}

/**
 * 获取指定下标的数组
 * @param $array
 * @param $key
 * @return array
 */
function getArrayValue($array,$key){
    $result = array();
    if(!is_array($array)){return $result;}
    foreach ($array as $val) {
        $result[] = @$val[$key];
    }
    return $result;

}

/**
 * 将数组某值作为下标
 * @param $array
 * @param $key
 * @return array
 */
function arrayIndex($array,$key){
    $result = array();
    if(!is_array($array)){return $result;}
    foreach ($array as $val) {
        $result[$val[$key]] = @$val;
    }
    return $result;
}

/**
 * 存储大小格式化
 * @param $size
 * @return string
 */
function formatSize($size){
    $a = array("B", "KB", "MB", "GB", "TB", "PB");
    $pos = 0;
    while ($size >= 1024) {
        $size /= 1024;
        $pos++;
    }
    return round($size,2).' '.$a[$pos];
}

/**
 * 时间格式化SNS方式
 * @param $time
 * @return bool|string
 */
function formatTime($time){
    $ntime = time();
    $diff = $ntime-$time;
    if($diff < 60){
        return "{$diff}秒前";
    }elseif($diff >= 60 && $diff < 3600){
        return intval($diff/60)."分钟前";
    }elseif($diff >= 3600 && $diff < 86400){
        return intval($diff/3660)."小时前";
    }else{
        return date("m-d H:i",$time);
    }
}


/**
 * 生成缩略图
 * @param $image 原图地址
 * @param $maxWidth 宽度
 * @param $maxHeight 高度
 * @param int $type
 *              1：进行等比缩放，不裁剪，
 *              2：限定缩略图的宽最少为<$maxWidth>，高最少为<$maxHeight>，进行等比缩放，居中裁剪，
 *              3：限定缩略图的宽最少为<$maxWidth>，高最少为<$maxHeight>,不裁剪
 *              5：按规定缩放，不裁剪,会有拉伸
 */
function thumb($image,$maxWidth,$maxHeight,$type=1){
    if(!$image){
        return '';
    }
    $maxWidth = abs(intval($maxWidth));
    $maxHeight = abs(intval($maxHeight));
    if($maxWidth == 0 || $maxHeight == 0){
        return $image;
    }
    $type = $type > 4 ? 1 : $type;
    $imgInfo = pathinfo($image);
    $dirname = $imgInfo['dirname'];
    $extension = $imgInfo['extension'];
    $filename = $imgInfo['filename'];

    $code = substr(MD5(THUMB_KEY.MD5($filename.$type).MD5(MD5($maxWidth).$maxHeight)),8,10);

    $thumbImage = ATTACH_URL.$dirname.'/'.$filename.'_'.$code.'_'.$type.'_'.$maxWidth.'_'.$maxHeight.'.'.$extension;
    return $thumbImage;
}

/**
 * 加密字符
 * @param $string
 * @param string $operation
 * @param string $key
 * @param int $expiry
 * @return string
 */
function authcode($string, $operation = 'DECODE', $key = '', $expiry = 0) {
    $ckey_length = 4;
    $key = md5($key != '' ? $key : AUTHKEY);
    $keya = md5(substr($key, 0, 16));
    $keyb = md5(substr($key, 16, 16));
    $keyc = $ckey_length ? ($operation == 'DECODE' ? substr($string, 0, $ckey_length): substr(md5(microtime()), -$ckey_length)) : '';

    $cryptkey = $keya.md5($keya.$keyc);
    $key_length = strlen($cryptkey);

    $string = $operation == 'DECODE' ? urlsafe_b64decode(substr($string, $ckey_length)) : sprintf('%010d', $expiry ? $expiry + time() : 0).substr(md5($string.$keyb), 0, 16).$string;
    $string_length = strlen($string);

    $result = '';
    $box = range(0, 255);

    $rndkey = array();
    for($i = 0; $i <= 255; $i++) {
        $rndkey[$i] = ord($cryptkey[$i % $key_length]);
    }

    for($j = $i = 0; $i < 256; $i++) {
        $j = ($j + $box[$i] + $rndkey[$i]) % 256;
        $tmp = $box[$i];
        $box[$i] = $box[$j];
        $box[$j] = $tmp;
    }

    for($a = $j = $i = 0; $i < $string_length; $i++) {
        $a = ($a + 1) % 256;
        $j = ($j + $box[$a]) % 256;
        $tmp = $box[$a];
        $box[$a] = $box[$j];
        $box[$j] = $tmp;
        $result .= chr(ord($string[$i]) ^ ($box[($box[$a] + $box[$j]) % 256]));
    }

    if($operation == 'DECODE') {
        if((substr($result, 0, 10) == 0 || substr($result, 0, 10) - time() > 0) && substr($result, 10, 16) == substr(md5(substr($result, 26).$keyb), 0, 16)) {
            return substr($result, 26);
        } else {
            return '';
        }
    } else {
        return $keyc.str_replace('=', '', urlsafe_b64encode($result));
    }

}

function urlsafe_b64encode($string) {
    $data = base64_encode($string);
    $data = str_replace(array('+','/','='),array('-','_',''),$data);
    return $data;
}
function urlsafe_b64decode($string) {
    $data = str_replace(array('-','_'),array('+','/'),$string);
    $mod4 = strlen($data) % 4;
    if ($mod4) {
        $data .= substr('====', $mod4);
    }
    return base64_decode($data);
}

/**
 * 加载资源文件
 * @param $file
 * @param $url
 * @param string $type
 */
function importRes($file){
    $str = "";
    if(is_string($file)){
        $file = array($file);
    }
    foreach($file As $val){
        $type = strtolower(pathinfo($val, PATHINFO_EXTENSION));
        switch($type){
            case 'js' :
                $str .= '<script src="'.$val.'?_v='.C('RES_VERSION').'"  type="text/javascript"></script>';
                break;
            case 'css' :
                $str .= '<link href="'.$val.'?_v='.C('RES_VERSION').'" rel="stylesheet" type="text/css"/>';
                break;
        }
    }
    return $str."\r\n";
}


/*
 * HTTP GET Request
*/
function get($url, $param = null) {
    if($param != null) {
        $query = http_build_query($param);
        $url = $url . '?' . $query;
    }
    $ch = curl_init();
    if(stripos($url, "https://") !== false){
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    }

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
    $content = curl_exec($ch);
    $status = curl_getinfo($ch);
    curl_close($ch);
    if(intval($status["http_code"]) == 200) {
        return $content;
    }else{
        echo $status["http_code"];
        return false;
    }
}

/*
 * HTTP POST Request
*/
function post($url, $params) {
    $ch = curl_init();
    if(stripos($url, "https://") !== false) {
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    }

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    $content = curl_exec($ch);
    $status = curl_getinfo($ch);
    curl_close($ch);
    if(intval($status["http_code"]) == 200) {
        return $content;
    } else {
        echo $status["http_code"];
        return false;
    }
}

function page($current_page,$page_total,$parameter = array(),$limit = 20){
    $page = new \Think\Page($page_total,$limit,$parameter);
    $page->setConfig('first','首页');
    $page->setConfig('last','尾页');
    $page->setConfig('prev','上一页');
    $page->setConfig('next','下一页');
    return $page->show();
}

function getHttpCode($httpCode){
    $httpCodes = array(
        '100' => '访问被拒绝',
        '101' => '访问被拒绝',
        '200' => '请求成功',
        '201' => '请求已经被实现',
        '202' => '服务器已接受请求，但尚未处理',
        '203' => '服务器已成功处理了请求',
        '204' => '服务器成功处理了请求',
        '205' => '服务器成功处理了请求',
        '206' => '服务器成功处理了请求',
        '301' => '网站地址被跳转',
        '302' => '网站地址被跳转',
        '303' => '网站地址被跳转',
        '304' => '网站地址被跳转',
        '305' => '被请求的资源必须通过指定的代理才能被访问',
        '400' => '当前请求无法被服务器理解',
        '401' => '当前请求需要用户验证',
        '403' => '服务器已经理解请求，但是拒绝执行它',
        '404' => '请求失败',
        '500' => '请求的服务器出错',
        '505' => '服务器不支持，或者拒绝支持在请求中使用的 HTTP 版本',
    );
    return isset($httpCodes[$httpCode]) ? $httpCodes[$httpCode] : '未知';
}

function getEnabled($enabled,$type = 1){
    $typeList = array(
        1 => array('Y' => '正常','N' => '禁用')
    );
    return $typeList[$type][$enabled];
}

?>