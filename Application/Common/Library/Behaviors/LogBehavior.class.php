<?php
 /**
 * +----------------------------------------------------------------------
 * | Author:  jiayin <caojiayin1984@gmail.com>
 * +----------------------------------------------------------------------
 * | 2015-11-16 11:28
 * +----------------------------------------------------------------------
 * | LogBehavior.class.php
 * +----------------------------------------------------------------------
 **/
namespace Common\Library\Behaviors;
/**
 * 后台操作日志,仅记录POST日志
 * Class LogBehavior
 * @package Common\Library\Behaviors
 */
class LogBehavior{

    public function run(&$param){
        if(IS_POST){ //是否是post提交
            $controller = strtolower(CONTROLLER_NAME);
            $action = strtolower(ACTION_NAME);
            $explain = json_encode(I('post.'));
            $other = json_encode(I('request.'));
            $operator_id = $param['operator_id'];
            M('system_log','',C("CP_DB_CONFIG"))->add(array(
                'controller' => $controller,
                'action' => $action,
                'operator_id' => $operator_id,
                'explain' => $explain,
                'other' => $other,
                'create_time' => time(),
            ));
        }
    }
}