<?php
 /**
 * +----------------------------------------------------------------------
 * | Author:  jiayin <caojiayin1984@gmail.com>
 * +----------------------------------------------------------------------
 * | 2015-11-16 11:17
 * +----------------------------------------------------------------------
 * | BaseModel.class.php
 * +----------------------------------------------------------------------
 **/
namespace Common\Library\Model;
use Think\Model;

/**
 * Model基类
 * Class BaseModel
 * @package Common\Library\Model
 */
class BaseModel extends Model{

    /**
     * 分页查询
     * @param array $parameter 分页参数
     */
    public function pageSelect($parameter = array()){
        $options = $this->options;
        $page = $options['page'];
        unset($this->options['page']);
        $dataTotal = $this->Count();
        $this->options['page'] = $page;
        if(!isset($options['limit'])){
            $options['limit'] = C('PAGE_LIMIT') ? C('PAGE_LIMIT') : 20;
        }
        if(!isset($options['page'])){
            $options['page'] = max(1,I(C('VAR_PAGE'),1,'intval'));
        }
        $totalPages = ceil($dataTotal / $options['limit']);
        if($totalPages < $options['page']){ //防止当前页码超出最大页码
            $options['page'] = $totalPages;
        }
        //  var_dump($options);
        $result = $this->select($options);
        $page = new \Think\Page($dataTotal,$options['limit'],$parameter);
        $page->setConfig('first','首页');
        $page->setConfig('last','尾页');
        $page->setConfig('prev','上一页');
        $page->setConfig('next','下一页');
        $total = count($result);
        $resultData = array(
            'items' => $result,
            'page' => $dataTotal > $total ? $page->show() : '',
            'total' => $total,
            'allcount' => $dataTotal,
            'listRows' => $this->options['limit'],
        );
        return $resultData;
    }
}