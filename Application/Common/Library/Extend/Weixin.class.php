<?php
 /**
 * +----------------------------------------------------------------------
 * | Author:  jiayin <caojiayin1984@gmail.com>
 * +----------------------------------------------------------------------
 * | 2015/10/30 10:47
 * +----------------------------------------------------------------------
 * | Weixin.class.php
 * +----------------------------------------------------------------------
 * | 微信接口
 * +----------------------------------------------------------------------
 **/
namespace Common\Library\Extend;
class Weixin{
    private $cacheName = "WEIXIN_CACHE_DATA";
    private $appId;
    private $appSecret;
    private $error = array();

    public function __construct($appId, $appSecret){
        $this->appId = $appId;
        $this->appSecret = $appSecret;
    }


    public function getUserOpenID($code){
        $url = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid='.$this->appId.'&secret='.$this->appSecret.'&code='.$code.'&grant_type=authorization_code';
        $res = json_decode($this->httpGet($url));
        if($res->errcode > 0){
            $this->setError($res->errorcode,$res->errmsg);
            return false;
        }
        $data['openid'] = $res->openid;
        $data['access_token'] = $res->access_token;
        $data['refresh_token'] = $res->refresh_token;
        $data['expires_in'] = time() + 7000;
        return $data;
    }

    public function getUserInfo($code){
        $userToken = $this->getUserOpenID($code);
        $url = 'https://api.weixin.qq.com/sns/userinfo?access_token='.$userToken['access_token'].'&openid='.$userToken['openid'].'&lang=zh_CN';
        $res = json_decode($this->httpGet($url));
        if($res->errcode > 0){
            $this->setError($res->errorcode,$res->errmsg);
            return false;
        }

        $data = array();
        $data['nickname'] = $res->nickname;
        $data['sex'] = $res->sex;
        $data['language'] = $res->language;
        $data['city'] = $res->city;
        $data['province'] = $res->province;
        $data['country'] = $res->country;
        $data['headimgurl'] = $res->headimgurl;
        return $data;
    }


    public function getSignPackage($url = '') {
        $jsapiTicket = $this->getJsApiTicket();
        if($jsapiTicket === false){return false;}
        $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
        $url = $url ? $url : "$protocol$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

        $timestamp = time();
        $nonceStr = $this->createNonceStr();
        $string = "jsapi_ticket=$jsapiTicket&noncestr=$nonceStr&timestamp=$timestamp&url=$url";
        $signature = sha1($string);
        $signPackage = array(
            "appId"     => $this->appId,
            "nonceStr"  => $nonceStr,
            "timestamp" => $timestamp,
            "url"       => $url,
            "signature" => $signature,
            "rawString" => $string
        );
        return $signPackage;
    }

    private function createNonceStr($length = 16) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $str = "";
        for ($i = 0; $i < $length; $i++) {
            $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
        }
        return $str;
    }

    private function getJsApiTicket() {
        //$data = json_decode(file_get_contents("jsapi_ticket.json"));
        $data = S($this->cacheName);
        if (!$data || ($data && $data['ticket']['expire_time'] < time())) {
            $accessToken = $this->getAccessToken();
            $data = S($this->cacheName);
            $url = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?type=jsapi&access_token=$accessToken";
            $res = json_decode($this->httpGet($url));
            if($res->errcode > 0){
                $this->setError($res->errorcode,$res->errmsg);
                return false;
            }
            $ticket = $res->ticket;
            $data['ticket']['expire_time'] = time() + 7000;
            $data['ticket']['jsapi_ticket'] = $ticket;
            S($this->cacheName,$data);
        } else {
            $ticket = $data['ticket']['jsapi_ticket'];
        }
        return $ticket;
    }

    private function getAccessToken() {
        $data = S($this->cacheName);
        //$data = json_decode(file_get_contents("access_token.json"));
        if (!$data || ($data && $data['access_token']['expire_time'] < time())) {
            // $url = "https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid=$this->appId&corpsecret=$this->appSecret";
            $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=$this->appId&secret=$this->appSecret";
            $res = json_decode($this->httpGet($url));
            if($res->errcode > 0){
                $this->setError($res->errorcode,$res->errmsg);
                return false;
            }
            $access_token = $res->access_token;
            $data['access_token']['expire_time'] = time() + 7000;
            $data['access_token']['access_token'] = $access_token;
            S($this->cacheName,$data);
        } else {
            $access_token = $data['access_token']['access_token'];
        }
        return $access_token;
    }

    private function httpGet($url) {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 500);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_URL, $url);
        $res = curl_exec($curl);
        curl_close($curl);
        return $res;
    }

    public function setError($errorcode,$errmsg){
        $this->error = array('errcode' => $errorcode,'errmsg' => $errmsg);
    }
    public function getError(){
        return $this->error;
    }
}