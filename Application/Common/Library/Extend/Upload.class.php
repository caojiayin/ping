<?php
 /**
 * +----------------------------------------------------------------------
 * | Author:  jiayin <caojiayin1984@gmail.com>
 * +----------------------------------------------------------------------
 * | 2015-11-18 10:24
 * +----------------------------------------------------------------------
 * | Upload.class.php
 * +----------------------------------------------------------------------
 **/
namespace Common\Library\Extend;
class Upload {
    private $imagExts = array(
        'image' => array('gif','jpg','png','jpeg'),
        'file' => array('gif','jpg','png','jpeg','bmp','xls','xlsx','doc','docx','ppt','pptx','rar','zip','7z','swf','txt'),
    );
    private $handle = null;
    private $error = '';

    public function __construct(){
        $fileName = $this->_createName();
        $this->handle = new \Think\Upload(array(
            'rootPath' => WEB_PATH.'res/',
            'subName' => date("Y/m/d"),
            'savePath' => 'attach/',
            'saveName' => $fileName,
        ));
    }

    /**
     * 保存文件
     * @param null $field 上传表单名
     * @param null $type 上传文件类型，image,file,video
     * @param null $fileName 保存文件名
     * @param bool $replace 相同文件是否覆盖，默认为覆盖
     */
    public function save($field,$type='image'){
        $files = $_FILES[$field];
        $result = array();
        if(is_array($files['name'])){
            foreach($files As $key => $val){
                $file = array(
                    'name' => $val,
                    'type' => $files['type'][$key],
                    'tmp_name' => $files['tmp_name'][$key],
                    'error' => $files['error'][$key],
                    'size' => $files['size'][$key],
                );
                $result[] = $this->_save($file,$type);
            }
        }else{
            $result = $this->_save($files,$type);
        }
        return $result;
    }
    public function getError(){
        return array('status' => 0,'info' => $this->error);
    }
    private function _save($file,$type){
        $ext = $this->getExt($file['name']);

        if($this->handle){
            /* 文件上传失败，捕获错误代码 */
            if ($file['error']) {
                $this->_error($file['error']);
                return $this->getError();
            }
            /* 无效上传 */
            if (empty($file['name'])){
                $this->error = '未知上传错误！';
            }
            /* 检查是否合法上传 */
            if (!is_uploaded_file($file['tmp_name'])) {
                $this->error = '非法上传文件！';
                return $this->getError();
            }
            /* 检查文件大小 */
            if (!$this->_checkSize($file['size'],$type)) {
                $this->error = '上传文件大小不符！';
                return $this->getError();
            }
            /* 检查文件后缀 */
            if (!$this->_checkExt($ext,$type)) {
                $this->error = '不允许上传'.$ext.'类型文件';
                return $this->getError();
            }
            if(false !== $result = $this->handle->uploadOne($file)){
                $result['name'] = $file['name'];
                $result['original'] = $result['savepath'].$result['savename'];
                $result['url'] = RES_URL.$result['original'];
                $result['type'] = $type;
                $result['is_image'] = in_array($ext,$this->imagExts['image']) ? 1 : 0;
                $result['size'] = $file['size'];
                $result['status'] = 1;
                /*if(WATER_POSITION){
                    $img = new \Think\Image();
                    $img->open(UPLOAD_PATH.$result['url']);
                    //$img->water(WATER_FILE,WATER_POSITION,100);
                    $img->save(UPLOAD_PATH.$result['url']);
                }*/
                return $result;
            }else{
                $this->error = $this->handle->getError();
                return $this->getError();
            }
        }else{
            $this->error = '未找到上传驱动组件';
            return $this->getError();
        }
    }
    /**
     * 获取错误代码信息
     * @param string $errorNo  错误号
     */
    private function _error($errorNo) {
        switch ($errorNo) {
            case 1:
                $this->error = '上传的文件超过了 php.ini 中 upload_max_filesize 选项限制的值！';
                break;
            case 2:
                $this->error = '上传文件的大小超过了 HTML 表单中 MAX_FILE_SIZE 选项指定的值！';
                break;
            case 3:
                $this->error = '文件只有部分被上传！';
                break;
            case 4:
                $this->error = '没有文件被上传！';
                break;
            case 6:
                $this->error = '找不到临时文件夹！';
                break;
            case 7:
                $this->error = '文件写入失败！';
                break;
            default:
                $this->error = '未知上传错误！';
        }
    }
    /**
     * 检查文件大小是否合法
     * @param integer $size 数据
     */
    private function _checkSize($size,$type = 'image') {
        $size = $size / 1024 / 1024;
        if($type == 'file'){
            return !($size > UPLOAD_FILE_SIZE) || (0 == UPLOAD_FILE_SIZE);
        }else{
            return !($size > UPLOAD_IMG_SIZE) || (0 == UPLOAD_IMG_SIZE);
        }
    }
    private function _checkExt($ext,$type){
        $exts = $this->imagExts[$type];
        if(!$exts){return false;}
        return in_array(strtolower($ext),$exts);
    }
    /**
     * 随机生成文件名称
     */
    private function _createName() {
        $charid = strtolower ( md5 ( uniqid ( rand (), true ) . rand(1111111,9999999).time()) );
        return $charid;
    }
    /**
     * 取得上传文件的后缀
     * @access private
     * @param string $filename 文件名
     * @return boolean
     */
    private function getExt($filename) {
        $pathinfo = pathinfo($filename);
        return strtolower($pathinfo['extension']);
    }

}