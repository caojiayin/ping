<?php
 /**
 * +----------------------------------------------------------------------
 * | Author:  jiayin <caojiayin1984@gmail.com>
 * +----------------------------------------------------------------------
 * | 2015/11/03 13:24
 * +----------------------------------------------------------------------
 * | Passport.class.php
 * +----------------------------------------------------------------------
 * | 第三方登录接口
 * +----------------------------------------------------------------------
 **/
namespace Common\Library\Extend;
class Passport{
    private static $handle = null;
    public function __construct($type){
        if(!(self::$handle instanceof self)){
            $this->_Connect($type);
        }
        return $this;
    }

    public function login($appkey, $callback, $scope='') {
        $state = md5(uniqid(rand(), TRUE));
        session("_PASSPORT_STATE_",$state);
        self::$handle->login($appkey, $callback,$state, $scope);
    }

    public function callback($appkey, $appsecretkey, $callback){
        $state = session("_PASSPORT_STATE_");
        session("_PASSPORT_STATE_",null);
        if($state != I("get.state")){
            E("无效回调数据");
        }
        self::$handle->callback($appkey, $appsecretkey, $callback);
    }

    public function get_user_info($token, $openid, $appkey = null, $format = "json"){
        return self::$handle->get_user_info($token, $openid, $appkey,$format);
    }

    private function _Connect($type){
        $dir = dirname(__FILE__);
        $type = parse_name(strtolower($type),1);
        $classFile = $dir.DIRECTORY_SEPARATOR."Passport".DIRECTORY_SEPARATOR.$type.".class.php";
        echo $classFile;
        if(!is_file($classFile)){
            E("{$type}.class.php文件不存在");
        }
        include($classFile);
        self::$handle = new $type();
        return '';
    }

}