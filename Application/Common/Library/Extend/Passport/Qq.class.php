<?php
 /**
 * +----------------------------------------------------------------------
 * | Author:  jiayin <caojiayin1984@gmail.com>
 * +----------------------------------------------------------------------
 * | 2015/11/03 13:55
 * +----------------------------------------------------------------------
 * | Qq.class.php
 * +----------------------------------------------------------------------
 * | QQ互联
 * +----------------------------------------------------------------------
 **/

class QQ{

    public function login($appkey, $callback,$state, $scope=''){
        $login_url = "https://graph.qq.com/oauth2.0/authorize?response_type=code&client_id="
            . $appkey . "&redirect_uri=" . urlencode($callback)
            . "&state=" . $state
            . "&scope=".$scope;
        redirect($login_url);
    }

    public function callback($appkey, $appsecretkey, $callback) {
        $code = I("get.code");
        $token = $this->get_access_token($appkey, $appsecretkey, $code, $callback);
        $openid = $this->get_openid($token);
        if(!$token || !$openid) {
            exit('get token or openid error!');
        }
        return array('openid' => $openid, 'token' => $token);
    }

    public function get_user_info($token, $openid, $appkey, $format = "json") {
        $url = "https://graph.qq.com/user/get_user_info";
        $param = array(
            "access_token"      =>    $token,
            "oauth_consumer_key"=>    $appkey,
            "openid"            =>    $openid,
            "format"            =>    $format
        );

        $response = get($url, $param);
        if($response == false) {
            return false;
        }

        $user = json_decode($response, true);
        return $user;
    }


    private function get_access_token($appkey, $appsecretkey, $code, $callback) {
        $url = "https://graph.qq.com/oauth2.0/token";
        $param = array(
            "grant_type"    =>    "authorization_code",
            "client_id"     =>    $appkey,
            "client_secret" =>    $appsecretkey,
            "code"          =>    $code,
            "redirect_uri"  =>    $callback
        );

        $response = get($url, $param);
        if($response == false) {
            return false;
        }
        $params = array();
        parse_str($response, $params);
        return $params["access_token"];
    }

    private function get_openid($access_token) {
        $url = "https://graph.qq.com/oauth2.0/me";
        $param = array(
            "access_token"    => $access_token
        );

        $response  = get($url, $param);
        if($response == false) {
            return false;
        }
        var_dump($response);
        if (strpos($response, "callback") !== false) {
            $lpos = strpos($response, "(");
            $rpos = strrpos($response, ")");
            $response  = substr($response, $lpos + 1, $rpos - $lpos -1);
        }

        $user = json_decode($response);
        if (isset($user->error) || $user->openid == "") {
            return false;
        }
        return $user->openid;
    }

}