-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        5.5.40 - MySQL Community Server (GPL)
-- 服务器操作系统:                      Win64
-- HeidiSQL 版本:                  9.1.0.4921
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 导出 ping 的数据库结构
CREATE DATABASE IF NOT EXISTS `ping` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `ping`;


-- 导出  表 ping.pi_manager 结构
CREATE TABLE IF NOT EXISTS `pi_manager` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL DEFAULT '0' COMMENT '所属权限,1-管理员，2-普通用户',
  `nickname` varchar(50) DEFAULT NULL COMMENT '昵称',
  `name` varchar(50) DEFAULT NULL COMMENT '姓名',
  `avatar` varchar(100) DEFAULT NULL COMMENT '头像',
  `username` varchar(20) DEFAULT NULL COMMENT '用户名',
  `password` varchar(32) DEFAULT NULL COMMENT '密码',
  `salt` varchar(10) DEFAULT NULL COMMENT '密码辅助',
  `last_time` int(11) DEFAULT NULL COMMENT '最后登录时间[timestamp]',
  `last_ip` varchar(15) DEFAULT NULL COMMENT '最后登录Ip',
  `enabled` enum('Y','N') NOT NULL DEFAULT 'Y' COMMENT '状态，1-可用，2-禁用',
  `is_default` enum('Y','N') NOT NULL DEFAULT 'N',
  `created_time` int(11) DEFAULT NULL COMMENT '创建时间[timestamp]',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='管理员';

-- 正在导出表  ping.pi_manager 的数据：1 rows
/*!40000 ALTER TABLE `pi_manager` DISABLE KEYS */;
INSERT INTO `pi_manager` (`id`, `role_id`, `nickname`, `name`, `avatar`, `username`, `password`, `salt`, `last_time`, `last_ip`, `enabled`, `is_default`, `created_time`) VALUES
	(9, 1, NULL, '管理员1', NULL, 'admin', 'd604969de4912d7de702e78dbee193aa', '387529', 1457251673, '127.0.0.1', 'Y', 'Y', 1457187235);
INSERT INTO `pi_manager` (`id`, `role_id`, `nickname`, `name`, `avatar`, `username`, `password`, `salt`, `last_time`, `last_ip`, `enabled`, `is_default`, `created_time`) VALUES
	(11, 2, NULL, 'dsgdfsg', NULL, 'asdasd', 'bf3b6d69fe98190b4ea9fe399b6ecf3f', '536919', NULL, NULL, 'Y', 'N', 1457250326);
/*!40000 ALTER TABLE `pi_manager` ENABLE KEYS */;


-- 导出  表 ping.pi_sites 结构
CREATE TABLE IF NOT EXISTS `pi_sites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `webname` varchar(50) NOT NULL DEFAULT '0',
  `website` varchar(50) NOT NULL DEFAULT '0',
  `port` smallint(6) NOT NULL DEFAULT '0',
  `last_time` int(11) NOT NULL DEFAULT '0',
  `ping` double NOT NULL DEFAULT '0',
  `http_status` smallint(6) NOT NULL DEFAULT '0',
  `status` enum('Y','N') NOT NULL DEFAULT 'Y',
  `atime` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `domain` (`website`),
  KEY `name` (`webname`),
  KEY `ping` (`ping`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='网站列表';

-- 正在导出表  ping.pi_sites 的数据：10 rows
/*!40000 ALTER TABLE `pi_sites` DISABLE KEYS */;
INSERT INTO `pi_sites` (`id`, `webname`, `website`, `port`, `last_time`, `ping`, `http_status`, `status`, `atime`) VALUES
	(1, '阿达是的1', 'www.ping.com', 80, 1457181505, 0, 200, 'Y', 1457151249);
INSERT INTO `pi_sites` (`id`, `webname`, `website`, `port`, `last_time`, `ping`, `http_status`, `status`, `atime`) VALUES
	(2, '百度', 'www.baidu.com', 443, 1457181508, 0.041, 200, 'Y', 1457151752);
INSERT INTO `pi_sites` (`id`, `webname`, `website`, `port`, `last_time`, `ping`, `http_status`, `status`, `atime`) VALUES
	(3, '开服网', 'www.kaifu.com', 80, 1457181512, 0.047, 200, 'Y', 1457151765);
INSERT INTO `pi_sites` (`id`, `webname`, `website`, `port`, `last_time`, `ping`, `http_status`, `status`, `atime`) VALUES
	(5, '开区网', 'www.kaiquwang.com', 80, 1457181556, 0.052, 200, 'Y', 1457154735);
INSERT INTO `pi_sites` (`id`, `webname`, `website`, `port`, `last_time`, `ping`, `http_status`, `status`, `atime`) VALUES
	(6, '微博', 'www.weibo.com', 80, 1457181559, 0.06, 302, 'Y', 1457154756);
INSERT INTO `pi_sites` (`id`, `webname`, `website`, `port`, `last_time`, `ping`, `http_status`, `status`, `atime`) VALUES
	(7, '知乎', 'www.zhihu.com', 80, 1457181562, 0.004, 200, 'Y', 1457155476);
INSERT INTO `pi_sites` (`id`, `webname`, `website`, `port`, `last_time`, `ping`, `http_status`, `status`, `atime`) VALUES
	(8, '腾讯', 'www.qq.com', 80, 1457181566, 0.032, 200, 'Y', 1457155639);
INSERT INTO `pi_sites` (`id`, `webname`, `website`, `port`, `last_time`, `ping`, `http_status`, `status`, `atime`) VALUES
	(9, 'bitbucket', 'www.bitbucket.org', 80, 1457181570, 0.277, 301, 'Y', 1457155669);
INSERT INTO `pi_sites` (`id`, `webname`, `website`, `port`, `last_time`, `ping`, `http_status`, `status`, `atime`) VALUES
	(10, '119.84.85.18', '119.84.85.18', 80, 1457181573, 0.006, 404, 'Y', 1457155864);
INSERT INTO `pi_sites` (`id`, `webname`, `website`, `port`, `last_time`, `ping`, `http_status`, `status`, `atime`) VALUES
	(11, '180.97.33.108', '180.97.33.108', 80, 1457181576, 0.04, 200, 'Y', 1457165046);
/*!40000 ALTER TABLE `pi_sites` ENABLE KEYS */;


-- 导出  表 ping.pi_sites_item 结构
CREATE TABLE IF NOT EXISTS `pi_sites_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sites_id` int(11) DEFAULT NULL,
  `ping` double DEFAULT NULL,
  `http_status` smallint(6) DEFAULT NULL,
  `atime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sites_id` (`sites_id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- 正在导出表  ping.pi_sites_item 的数据：22 rows
/*!40000 ALTER TABLE `pi_sites_item` DISABLE KEYS */;
INSERT INTO `pi_sites_item` (`id`, `sites_id`, `ping`, `http_status`, `atime`) VALUES
	(1, 1, 0, 200, 1457180159);
INSERT INTO `pi_sites_item` (`id`, `sites_id`, `ping`, `http_status`, `atime`) VALUES
	(2, 2, 0.052, 200, 1457180162);
INSERT INTO `pi_sites_item` (`id`, `sites_id`, `ping`, `http_status`, `atime`) VALUES
	(3, 3, 0.049, 200, 1457180166);
INSERT INTO `pi_sites_item` (`id`, `sites_id`, `ping`, `http_status`, `atime`) VALUES
	(4, 4, 0, 0, 1457180206);
INSERT INTO `pi_sites_item` (`id`, `sites_id`, `ping`, `http_status`, `atime`) VALUES
	(5, 5, 0.109, 200, 1457180210);
INSERT INTO `pi_sites_item` (`id`, `sites_id`, `ping`, `http_status`, `atime`) VALUES
	(6, 6, 0.069, 302, 1457180213);
INSERT INTO `pi_sites_item` (`id`, `sites_id`, `ping`, `http_status`, `atime`) VALUES
	(7, 7, 0.017, 200, 1457180217);
INSERT INTO `pi_sites_item` (`id`, `sites_id`, `ping`, `http_status`, `atime`) VALUES
	(8, 8, 0.049, 200, 1457180221);
INSERT INTO `pi_sites_item` (`id`, `sites_id`, `ping`, `http_status`, `atime`) VALUES
	(9, 9, 0.354, 301, 1457180225);
INSERT INTO `pi_sites_item` (`id`, `sites_id`, `ping`, `http_status`, `atime`) VALUES
	(10, 10, 0.007, 404, 1457180228);
INSERT INTO `pi_sites_item` (`id`, `sites_id`, `ping`, `http_status`, `atime`) VALUES
	(11, 11, 0.047, 200, 1457180231);
INSERT INTO `pi_sites_item` (`id`, `sites_id`, `ping`, `http_status`, `atime`) VALUES
	(12, 1, 0, 200, 1457181505);
INSERT INTO `pi_sites_item` (`id`, `sites_id`, `ping`, `http_status`, `atime`) VALUES
	(13, 2, 0.041, 200, 1457181508);
INSERT INTO `pi_sites_item` (`id`, `sites_id`, `ping`, `http_status`, `atime`) VALUES
	(14, 3, 0.047, 200, 1457181512);
INSERT INTO `pi_sites_item` (`id`, `sites_id`, `ping`, `http_status`, `atime`) VALUES
	(15, 4, 0, 0, 1457181552);
INSERT INTO `pi_sites_item` (`id`, `sites_id`, `ping`, `http_status`, `atime`) VALUES
	(16, 5, 0.052, 200, 1457181556);
INSERT INTO `pi_sites_item` (`id`, `sites_id`, `ping`, `http_status`, `atime`) VALUES
	(17, 6, 0.06, 302, 1457181559);
INSERT INTO `pi_sites_item` (`id`, `sites_id`, `ping`, `http_status`, `atime`) VALUES
	(18, 7, 0.004, 200, 1457181562);
INSERT INTO `pi_sites_item` (`id`, `sites_id`, `ping`, `http_status`, `atime`) VALUES
	(19, 8, 0.032, 200, 1457181566);
INSERT INTO `pi_sites_item` (`id`, `sites_id`, `ping`, `http_status`, `atime`) VALUES
	(20, 9, 0.277, 301, 1457181570);
INSERT INTO `pi_sites_item` (`id`, `sites_id`, `ping`, `http_status`, `atime`) VALUES
	(21, 10, 0.006, 404, 1457181573);
INSERT INTO `pi_sites_item` (`id`, `sites_id`, `ping`, `http_status`, `atime`) VALUES
	(22, 11, 0.04, 200, 1457181576);
/*!40000 ALTER TABLE `pi_sites_item` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
